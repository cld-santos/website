from django.http import HttpResponse
from django.template import loader, Context
from django.views import View
from .reader import FolderExplorer


class Posts(View):
    def __init__(self, path, root_url, template='posts.html'):
        self.path = path
        self.root_url = root_url
        self.template = template

    def get(self, request):
        xavier = FolderExplorer(path=self.path)
        posts = xavier.read()
        posts = sorted(posts, key=lambda post: post.last_update, reverse=True)

        template = loader.get_template(self.template)
        rendered_template = template.render(
            Context({
                'posts': posts,
                'root_url': self.root_url
            })
        )
        return HttpResponse(rendered_template)


class Post(View):
    def __init__(self, path,  template='post.html'):
        self.path = path
        self.template = template

    def get(self, request, *args, **kwargs):
        post_name = kwargs.get('name')
        xavier = FolderExplorer(path=self.path)
        post = xavier.read_file(post_name)
        
        template = loader.get_template(self.template)
        rendered_template = template.render(Context({'post': post}))
        return HttpResponse(rendered_template)


class Thoughts(Posts):
    def __init__(self):
        super().__init__(path='static/thoughts/', root_url='thought')


class Thought(Post):
    def __init__(self):
        super().__init__(path='static/thoughts/')


class Portifolios(Posts):
    def __init__(self):
        super().__init__(path='static/portifolio/', root_url='portifolio')


class Portifolio(Post):
    def __init__(self):
        super().__init__(path='static/portifolio/')


class AboutMe(View):
    def get(self, request):
        template = loader.get_template('about-me.html')
        rendered_template = template.render(Context())
        return HttpResponse(rendered_template)
